$(document).ready(function (){
    $('.owl-carousel').owlCarousel({
      loop: true,
      margin:20,
      nav:false,
      dotsEach: true,
      dotData: true,
      responsiveClass: true,
      autoplay:false ,
      slideSpeed : 3000,
      autoplayTimeout:3000,
      autoplayHoverPause:true,    
      owldots:true,
      URLhashListener:true,
      autoplayHoverPause:true,
      startPosition: 'URLHash',
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
        }
      }
    });

  });

  $(document).ready(function (){
    $('.service__sliders').owlCarousel({
      loop: true,
      margin:20,
      nav:false,
      dotsEach: true,
      dotData: true,
      responsiveClass: true,
      autoplay:false,
      slideSpeed : 2000,
      autoplayTimeout:2000,
      autoplayHoverPause:true,    
      owldots:true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 3,
          nav: false
        },
        1000: {
          items: 3,
        }
      }
    });

  });
   

  $(document).ready(function (){
    $('.client__slider').owlCarousel({
      loop: true,
      margin:140,
      nav:false,
      dotsEach: true,
      dotData: true,
      responsiveClass: true,
      autoplay:false,
      autoplaySpeed: 5000,
      slideTransition: 'linear',
      autoplayTimeout: 5000,
      autoplayHoverPause:true,    
      owldots:true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 3,
          nav: false
        },
        1000: {
          items: 3,
        }
      }
    });

  });


 $(document).ready(function () {
  $(window).scroll(function() {
    var sticky = $('.menu_top'),
      scroll = $(window).scrollTop();
     
    if (scroll >= 40) { 
      sticky.addClass('fixed_menu'); }
    else { 
     sticky.removeClass('fixed_menu');
  
  }
  });
  $('.mobile_bar').click(function(){
    $('#mainNav').toggleClass('menu_Active');
    $(this).toggleClass('make_cross');
  });
});
